.. |company| replace:: NT System Work

.. |company_logo| image:: http://ntsystemwork.com/wp-content/uploads/2018/03/NT_System_Work.jpg
   :alt: jeo Soft
   :target: https://www.ntsystemwork.com.ar

.. image:: https://img.shields.io/badge/license-AGPL--3-blue.png
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3

=========
CASH FLOW
=========


Este modulo permite consultar el flujo de caja por fecha para efecto de analisis
de cobros y adicionalmente permite modificar la fecha de vencimiento de las
facturas de clientes y proveedores con el motivo de evitar pagos morosos de
clientes y proveedores.

Installation
============

To install this module, you need to:

#. Just intall it.

Configuration
=============

To configure this module, you need to:

#. No configuration needed

Bug Tracker
===========

Bugs are tracked on `GitHub Issues
<https://github.com/jobiols/[reponame]/issues>`_. In case of trouble, please
check there if your issue has already been reported. If you spotted it first,
help us smashing it by providing a detailed and welcomed feedback.

Credits
=======
Jorge Obiols <jorge.obiols@ntsystemwork.com.ar>


Contributors
------------

Maintainer
----------

|company_logo|

This module is maintained by |company|.
